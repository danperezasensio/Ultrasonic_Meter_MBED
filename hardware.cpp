#include "mbed.h"
#include "DebounceIn.h"

BusOut display(p10,p9,p6,p7,p8,p11,p12); // Segmentos a,b,c,d,e,f,g (dp no se ha conectado).

DigitalOut display1(p20);				// Pin dedicado a dar alimentación al display 1 (IZQUIERDA).
DigitalOut display2(p19);				// Pin dedicado a dar alimentación al display 2 (DERECHA).

DebounceIn medir(p21);					// Pin dedicado a medir nivel bajo o nivel alto en el botón MEDIR, evitando el rebote del pulsador.
DigitalOut midiendoLED(p18);		// Pin dedicado a dar alimentación al led MIDIENDO.

DebounceIn modo(p22);						// Pin dedicado a medir nivel bajo o nivel alto en el botón MODO, evitando el rebote del pulsador.
DigitalOut modoLED(p17);				// Pin dedicado a dar alimentación al led MODO.

DebounceIn escala(p23);					// Pin dedicado a medir nivel bajo o nivel alto en el botón ESCALA, evitando el rebote del pulsador.
DigitalOut escalaLED(p16);			// Pin dedicado a dar alimentación al led ESCALA.
